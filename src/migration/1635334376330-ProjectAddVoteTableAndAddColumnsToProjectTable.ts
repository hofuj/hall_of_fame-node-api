import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectAddVoteTableAndAddColumnsToProjectTable1635334376330 implements MigrationInterface {
    name = 'ProjectAddVoteTableAndAddColumnsToProjectTable1635334376330';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "project"."vote" ("ip" character varying NOT NULL, "value" integer NOT NULL, "projectId" integer, CONSTRAINT "PK_3399d2de2d24af294bedc6a1d0c" PRIMARY KEY ("ip"))`,
        );
        await queryRunner.query(`ALTER TABLE "project"."project" ADD "codeUrl" character varying`);
        await queryRunner.query(`ALTER TABLE "project"."project" ADD "websiteUrl" character varying`);
        await queryRunner.query(
            `ALTER TABLE "project"."vote" ADD CONSTRAINT "FK_e3ff04f9e29ee9df1ff53abc028" FOREIGN KEY ("projectId") REFERENCES "project"."project"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."project" DROP CONSTRAINT "FK_d0474b642dc0ae63660dd8e2ac0"`);
        await queryRunner.query(`ALTER TABLE "project"."vote" DROP CONSTRAINT "FK_e3ff04f9e29ee9df1ff53abc028"`);
        await queryRunner.query(`ALTER TABLE "project"."project" DROP COLUMN "websiteUrl"`);
        await queryRunner.query(`ALTER TABLE "project"."project" DROP COLUMN "codeUrl"`);
        await queryRunner.query(`DROP TABLE "project"."vote"`);
    }
}
