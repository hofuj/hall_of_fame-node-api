import { MigrationInterface, QueryRunner } from 'typeorm';

export class CategoryAddCategoryTable1635110337215 implements MigrationInterface {
    name = 'CategoryAddCategoryTable1635110337215';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE SCHEMA IF NOT EXISTS "category"; CREATE TABLE "category"."category" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, CONSTRAINT "PK_9c4e4a89e3674fc9f382d733f03" PRIMARY KEY ("id"))`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "category"."category"`);
    }
}
