import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectChangeIsActiveToIsAccepted1635112920504 implements MigrationInterface {
    name = 'ProjectChangeIsActiveToIsAccepted1635112920504';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."project" RENAME COLUMN "isActive" TO "isAccepted"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."project" RENAME COLUMN "isAccepted" TO "isActive"`);
    }
}
