import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectAddProjectAndImageTable1635112715537 implements MigrationInterface {
    name = 'ProjectAddProjectAndImageTable1635112715537';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE SCHEMA IF NOT EXISTS "project"; CREATE TABLE "project"."project" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "description" character varying NOT NULL, "isActive" boolean NOT NULL DEFAULT false, "teamId" integer, CONSTRAINT "REL_d0474b642dc0ae63660dd8e2ac" UNIQUE ("teamId"), CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57" PRIMARY KEY ("id"))`,
        );
        await queryRunner.query(
            `CREATE SCHEMA IF NOT EXISTS "media"; CREATE TABLE "media"."image" ("id" SERIAL NOT NULL, "url" character varying NOT NULL, "projectId" integer, CONSTRAINT "PK_d6db1ab4ee9ad9dbe86c64e4cc3" PRIMARY KEY ("id"))`,
        );
        await queryRunner.query(
            `ALTER TABLE "project"."project" ADD CONSTRAINT "FK_d0474b642dc0ae63660dd8e2ac0" FOREIGN KEY ("teamId") REFERENCES "team"."team"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
        await queryRunner.query(
            `ALTER TABLE "media"."image" ADD CONSTRAINT "FK_63c8a589bd02b3525546ac70795" FOREIGN KEY ("projectId") REFERENCES "project"."project"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "media"."image" DROP CONSTRAINT "FK_63c8a589bd02b3525546ac70795"`);
        await queryRunner.query(`ALTER TABLE "project"."project" DROP CONSTRAINT "FK_d0474b642dc0ae63660dd8e2ac0"`);
        await queryRunner.query(`DROP TABLE "media"."image"`);
        await queryRunner.query(`DROP TABLE "project"."project"`);
    }
}
