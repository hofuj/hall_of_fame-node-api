import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectAddUpdatedAtColumn1635894559731 implements MigrationInterface {
    name = 'ProjectAddUpdatedAtColumn1635894559731';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."project" ADD "updatedAt" TIMESTAMP NOT NULL DEFAULT NOW()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."project" DROP COLUMN "updatedAt"`);
    }
}
