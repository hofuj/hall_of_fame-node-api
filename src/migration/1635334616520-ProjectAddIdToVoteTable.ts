import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectAddIdToVoteTable1635334616520 implements MigrationInterface {
    name = 'ProjectAddIdToVoteTable1635334616520';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."vote" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "project"."vote" DROP CONSTRAINT "PK_3399d2de2d24af294bedc6a1d0c"`);
        await queryRunner.query(
            `ALTER TABLE "project"."vote" ADD CONSTRAINT "PK_f18f52a0f589da5546cac0dcfbf" PRIMARY KEY ("ip", "id")`,
        );
        await queryRunner.query(`ALTER TABLE "project"."vote" DROP CONSTRAINT "PK_f18f52a0f589da5546cac0dcfbf"`);
        await queryRunner.query(
            `ALTER TABLE "project"."vote" ADD CONSTRAINT "PK_2d5932d46afe39c8176f9d4be72" PRIMARY KEY ("id")`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."vote" DROP CONSTRAINT "PK_2d5932d46afe39c8176f9d4be72"`);
        await queryRunner.query(
            `ALTER TABLE "project"."vote" ADD CONSTRAINT "PK_f18f52a0f589da5546cac0dcfbf" PRIMARY KEY ("ip", "id")`,
        );
        await queryRunner.query(`ALTER TABLE "project"."vote" DROP CONSTRAINT "PK_f18f52a0f589da5546cac0dcfbf"`);
        await queryRunner.query(
            `ALTER TABLE "project"."vote" ADD CONSTRAINT "PK_3399d2de2d24af294bedc6a1d0c" PRIMARY KEY ("ip")`,
        );
        await queryRunner.query(`ALTER TABLE "project"."vote" DROP COLUMN "id"`);
    }
}
