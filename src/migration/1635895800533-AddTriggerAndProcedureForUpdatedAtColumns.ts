import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddTriggerAndProcedureForUpdatedAtColumns1635895800533 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE FUNCTION entity_updated() RETURNS TRIGGER
                    LANGUAGE plpgsql
                    AS $$
            BEGIN
                NEW."updatedAt" := current_timestamp;
                RETURN NEW;
            END;
            $$;
            
            CREATE TRIGGER trigger_project_updated
                BEFORE UPDATE ON "project"."project"
                FOR EACH ROW
                EXECUTE PROCEDURE entity_updated();
        `);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental,@typescript-eslint/no-empty-function
    public async down(queryRunner: QueryRunner): Promise<void> {}
}
