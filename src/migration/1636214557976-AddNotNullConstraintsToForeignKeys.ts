import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddNotNullConstraintsToForeignKeys1636214557976 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "project"."project" ALTER COLUMN "teamId" SET NOT NULL;
            
            ALTER TABLE "project"."project" ALTER COLUMN "categoryId" SET NOT NULL;
            
            ALTER TABLE "project"."vote" ALTER COLUMN "projectId" SET NOT NULL;
            
            ALTER TABLE "media"."image" ALTER COLUMN "projectId" SET NOT NULL;
            
            ALTER TABLE "team"."team_members_member" ALTER COLUMN "teamId" SET NOT NULL;
            
            ALTER TABLE "team"."team_members_member" ALTER COLUMN "memberId" SET NOT NULL;

        `);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental,@typescript-eslint/no-empty-function
    public async down(queryRunner: QueryRunner): Promise<void> {}
}
