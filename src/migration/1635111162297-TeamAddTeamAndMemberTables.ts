import { MigrationInterface, QueryRunner } from 'typeorm';

export class TeamAddTeamAndMemberTables1635111162297 implements MigrationInterface {
    name = 'TeamAddTeamAndMemberTables1635111162297';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE SCHEMA IF NOT EXISTS "team"; CREATE TABLE "team"."member" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "surname" character varying NOT NULL, "personalWebsiteUrl" character varying, CONSTRAINT "PK_97cbbe986ce9d14ca5894fdc072" PRIMARY KEY ("id"))`,
        );
        await queryRunner.query(
            `CREATE TABLE "team"."team" ("id" SERIAL NOT NULL, "name" character varying, CONSTRAINT "PK_f57d8293406df4af348402e4b74" PRIMARY KEY ("id"))`,
        );
        await queryRunner.query(
            `CREATE TABLE "team"."team_members_member" ("teamId" integer NOT NULL, "memberId" integer NOT NULL, CONSTRAINT "PK_cd2c39ffba0333544fbc3f14568" PRIMARY KEY ("teamId", "memberId"))`,
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_8b14b1e030ab611ddf3cfd2376" ON "team"."team_members_member" ("teamId") `,
        );
        await queryRunner.query(
            `CREATE INDEX "IDX_b30e20c9b832e99016ba61f3ac" ON "team"."team_members_member" ("memberId") `,
        );
        await queryRunner.query(
            `ALTER TABLE "team"."team_members_member" ADD CONSTRAINT "FK_8b14b1e030ab611ddf3cfd2376f" FOREIGN KEY ("teamId") REFERENCES "team"."team"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
        );
        await queryRunner.query(
            `ALTER TABLE "team"."team_members_member" ADD CONSTRAINT "FK_b30e20c9b832e99016ba61f3acf" FOREIGN KEY ("memberId") REFERENCES "team"."member"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `ALTER TABLE "team"."team_members_member" DROP CONSTRAINT "FK_b30e20c9b832e99016ba61f3acf"`,
        );
        await queryRunner.query(
            `ALTER TABLE "team"."team_members_member" DROP CONSTRAINT "FK_8b14b1e030ab611ddf3cfd2376f"`,
        );
        await queryRunner.query(`DROP INDEX "team"."IDX_b30e20c9b832e99016ba61f3ac"`);
        await queryRunner.query(`DROP INDEX "team"."IDX_8b14b1e030ab611ddf3cfd2376"`);
        await queryRunner.query(`DROP TABLE "team"."team_members_member"`);
        await queryRunner.query(`DROP TABLE "team"."team"`);
        await queryRunner.query(`DROP TABLE "team"."member"`);
    }
}
