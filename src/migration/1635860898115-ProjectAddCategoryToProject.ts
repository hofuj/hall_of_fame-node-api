import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectAddCategoryToProject1635860898115 implements MigrationInterface {
    name = 'ProjectAddCategoryToProject1635860898115';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."project" ADD "categoryId" integer`);
        await queryRunner.query(
            `ALTER TABLE "project"."project" ADD CONSTRAINT "FK_3caef906211aad45559039f11f9" FOREIGN KEY ("categoryId") REFERENCES "category"."category"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."project" DROP CONSTRAINT "FK_3caef906211aad45559039f11f9"`);
        await queryRunner.query(`ALTER TABLE "project"."project" DROP COLUMN "categoryId"`);
    }
}
