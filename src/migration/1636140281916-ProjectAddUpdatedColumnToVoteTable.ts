import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectAddUpdatedColumnToVoteTable1636140281916 implements MigrationInterface {
    name = 'ProjectAddUpdatedColumnToVoteTable1636140281916';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."vote" ADD "updatedAt" TIMESTAMP NOT NULL DEFAULT NOW()`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "project"."vote" DROP COLUMN "updatedAt"`);
    }
}
