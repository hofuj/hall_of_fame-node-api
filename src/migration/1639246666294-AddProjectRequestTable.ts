import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddProjectRequestTable1639246666294 implements MigrationInterface {
    name = 'AddProjectRequestTable1639246666294';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE TABLE "project"."project_request" ("id" SERIAL NOT NULL, "identity" character varying NOT NULL, "updatedAt" TIMESTAMP NOT NULL DEFAULT NOW(), CONSTRAINT "PK_ba9211d59995f2e798914c9403c" PRIMARY KEY ("id"))`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "project"."project_request"`);
    }
}
