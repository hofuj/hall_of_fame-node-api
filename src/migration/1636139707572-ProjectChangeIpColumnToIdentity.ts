import { MigrationInterface, QueryRunner } from 'typeorm';

export class ProjectChangeIpColumnToIdentity1636139707572 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "project"."vote"
            RENAME COLUMN "ip" TO "identity";
        `);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental,@typescript-eslint/no-empty-function
    public async down(queryRunner: QueryRunner): Promise<void> {}
}
