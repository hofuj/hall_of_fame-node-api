import { MigrationInterface, QueryRunner } from 'typeorm';

export class SeedCategoryTable1635111108224 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        INSERT INTO "category"."category" (name) VALUES 
        ('Inżynieria Oprogramowania'),
        ('Testowanie Oprogramowania')
    `);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental,@typescript-eslint/no-empty-function
    public async down(queryRunner: QueryRunner): Promise<void> {}
}
