import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddPgcryptoModule1636197241921 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE EXTENSION IF NOT EXISTS pgcrypto;
        `);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental,@typescript-eslint/no-empty-function
    public async down(queryRunner: QueryRunner): Promise<void> {}
}
