import { MigrationInterface, QueryRunner } from 'typeorm';

export class UsrAddUserTable1635069742895 implements MigrationInterface {
    name = 'UsrAddUserTable1635069742895';

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(
            `CREATE SCHEMA IF NOT EXISTS "usr"; CREATE TABLE "usr"."user" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "password" character varying NOT NULL, "isActive" boolean NOT NULL DEFAULT false, CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id"))`,
        );
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "usr"."user"`);
    }
}
