import { MigrationInterface, QueryRunner } from 'typeorm';

export class UsrUserChangeNameToUsername1635072725767 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "usr"."user" RENAME COLUMN name TO username
        `);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental,@typescript-eslint/no-empty-function
    public async down(queryRunner: QueryRunner): Promise<void> {}
}
