import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { getManager } from 'typeorm';

import { User } from '../admin/user.entity';
import { Identity } from '../common/interfaces/identity.interface';
import { LoggedUserDto } from './DTOs/logged-user-dto';

@Injectable()
export class AuthService {
    constructor(private readonly jwtService: JwtService) {}

    public generateTokenForUser(user: User): string {
        return this.jwtService.sign({
            username: user.username,
            sub: user.id,
            isActive: user.isActive,
        });
    }

    public async hashPassword(password: string): Promise<string> {
        return await bcrypt.hash(password, 10);
    }

    public login(user: User): LoggedUserDto {
        return {
            userId: user.id,
            token: this.generateTokenForUser(user),
            isActive: user.isActive,
        };
    }

    public getIdentityString(identity: Identity): string {
        return `${identity.ip} ${identity.agent}`;
    }

    public async hashIdentityString(identityString: string): Promise<string> {
        const entityManager = getManager();
        return (
            await entityManager.query(
                `
            SELECT crypt($1, gen_salt('bf', 8));
        `,
                [identityString],
            )
        )[0].crypt;
    }
}
