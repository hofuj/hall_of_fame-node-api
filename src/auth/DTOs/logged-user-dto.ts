import { ApiProperty } from '@nestjs/swagger';

export class LoggedUserDto {
    @ApiProperty()
    userId: number;

    @ApiProperty()
    token: string;

    @ApiProperty()
    isActive: boolean;
}
