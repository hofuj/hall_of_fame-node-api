import { Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { CurrentUser } from '../common/decorators/current-user.decorator';
import { BusinessErrorMessage } from '../common/enums/business-error-message.enum';
import { AuthService } from './auth.service';
import { LoggedUserDto } from './DTOs/logged-user-dto';
import { LoginDto } from './DTOs/login.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @UseGuards(AuthGuard('local'))
    @Post('login')
    @ApiOkResponse({
        type: LoggedUserDto,
        description: 'User was logged in successfully.',
    })
    @ApiNotFoundResponse({
        description: BusinessErrorMessage.WRONG_CREDENTIALS,
    })
    @ApiBody({
        type: [LoginDto],
    })
    login(@CurrentUser() user): LoggedUserDto {
        return this.authService.login(user);
    }
}
