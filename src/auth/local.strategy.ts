import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Strategy } from 'passport-local';
import { Repository } from 'typeorm';

import { User } from '../admin/user.entity';
import { BusinessErrorMessage } from '../common/enums/business-error-message.enum';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
    ) {
        super();
    }

    public async validate(username: string, password: string): Promise<User> {
        const user = await this.userRepository.findOne({
            where: { username },
        });

        if (!user) {
            throw new HttpException(BusinessErrorMessage.WRONG_CREDENTIALS, HttpStatus.NOT_FOUND);
        }

        if (!(await bcrypt.compare(password, user.password))) {
            throw new HttpException(BusinessErrorMessage.WRONG_CREDENTIALS, HttpStatus.NOT_FOUND);
        }

        return user;
    }
}
