import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ThrottlerModule } from '@nestjs/throttler';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AdminModule } from './admin/admin.module';
import { AuthModule } from './auth/auth.module';
import { MediaModule } from './media/media.module';
import { ProjectModule } from './project/project.module';
import { VoteModule } from './vote/vote.module';

@Module({
    imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRoot(),
        ThrottlerModule.forRoot({
            ttl: 60,
            limit: 15,
        }),
        AuthModule,
        AdminModule,
        MediaModule,
        ProjectModule,
        VoteModule,
    ],
})
export class AppModule {}
