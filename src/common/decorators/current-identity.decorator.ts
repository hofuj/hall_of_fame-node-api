import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import { Identity } from '../interfaces/identity.interface';

// eslint-disable-next-line @typescript-eslint/no-unused-vars-experimental
export const CurrentIdentity = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const identity: Identity = {
        ip: request.ip,
        agent: request.headers['user-agent'],
    };

    return identity;
});
