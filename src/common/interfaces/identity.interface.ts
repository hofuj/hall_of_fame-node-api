export interface Identity {
    ip: string;
    agent: string;
}
