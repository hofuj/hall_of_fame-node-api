import { ApiProperty } from '@nestjs/swagger';

export class BooleanResponse {
    @ApiProperty()
    success: boolean;
}
