import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthModule } from '../auth/auth.module';
import { Project } from '../project/project.entity';
import { VoteController } from './vote.controller';
import { Vote } from './vote.entity';
import { VoteService } from './vote.service';

@Module({
    imports: [TypeOrmModule.forFeature([Vote, Project]), AuthModule],
    providers: [VoteService],
    exports: [VoteService],
    controllers: [VoteController],
})
export class VoteModule {}
