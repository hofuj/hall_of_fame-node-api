import { ApiProperty } from '@nestjs/swagger';

export class VoteDto {
    @ApiProperty()
    value: 1 | 0 | -1;
}
