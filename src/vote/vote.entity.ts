import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Project } from '../project/project.entity';

@Entity({ schema: 'project' })
export class Vote {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    identity: string;

    @Column()
    value: number;

    @Column({ name: 'updatedAt', type: 'timestamp', default: () => 'NOW()' })
    @ApiProperty()
    updatedAt: Date;

    @ManyToOne(() => Project, (project) => project.votes)
    project: Project;

    projectId: number;
}
