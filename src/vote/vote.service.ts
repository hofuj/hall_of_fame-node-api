import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';

import { AuthService } from '../auth/auth.service';
import { BusinessErrorMessage } from '../common/enums/business-error-message.enum';
import { Identity } from '../common/interfaces/identity.interface';
import { Project } from '../project/project.entity';
import { VotesInterface } from './interfaces/votes.interface';
import { Vote } from './vote.entity';

@Injectable()
export class VoteService {
    constructor(
        @InjectRepository(Vote) private readonly voteRepository: Repository<Vote>,
        @InjectRepository(Project) private readonly projectRepository: Repository<Project>,
        private readonly authService: AuthService,
    ) {}

    async getVoteCountByProjectId(projectId: number): Promise<number> {
        const sumQuery = await this.voteRepository
            .createQueryBuilder('v')
            .select('SUM(v.value)', 'sum')
            .innerJoin('v.project', 'p')
            .where('p.id=:id', { id: projectId })
            .execute();

        return Number(sumQuery[0].sum);
    }

    async getVoteCounts(): Promise<VotesInterface[]> {
        const entityManager = getManager();
        const result = await entityManager.query(`
            SELECT SUM(value) sum, "projectId"
            FROM project.vote
            GROUP BY "projectId";
        `);

        return result.map((r) => ({
            projectId: r.projectId,
            votes: Number(r.sum),
        }));
    }

    async vote(identity: Identity, value: number, projectId: number) {
        const identityString = this.authService.getIdentityString(identity);
        let vote = await this.getUserVote(identityString, projectId);

        if (vote && vote.value === value) {
            throw new HttpException(BusinessErrorMessage.YOU_ALREADY_VOTED, HttpStatus.BAD_REQUEST);
        }

        if (!vote && value === 0) {
            throw new HttpException(BusinessErrorMessage.YOU_NEVER_VOTED, HttpStatus.BAD_REQUEST);
        }

        if (vote && value === 0) {
            return !!(await this.voteRepository.remove(vote));
        }

        const project = await this.projectRepository.findOne(projectId);

        if (!vote) {
            const identityHash = await this.authService.hashIdentityString(identityString);
            vote = this.voteRepository.create({
                identity: identityHash,
                value,
                project,
            });
        } else {
            vote.value = value;
        }

        return !!(await this.voteRepository.save(vote));
    }

    async getUserVoteValue(identityString: string, projectId: number): Promise<number> {
        const entityManager = getManager();
        const vote = (
            await entityManager.query(
                `
            SELECT *
            FROM project.vote
            WHERE identity = crypt($1, identity) AND "projectId" = $2;
        `,
                [identityString, projectId],
            )
        )[0];

        if (!vote) {
            return 0;
        }

        return vote.value;
    }

    async getUserVotes(identityString: string): Promise<Vote[]> {
        const entityManager = getManager();
        return await entityManager.query(
            `
            SELECT *
            FROM project.vote
            WHERE identity = crypt($1, identity);
        `,
            [identityString],
        );
    }

    async getUserVote(identityString: string, projectId: number): Promise<Vote> {
        const entityManager = getManager();
        return (
            await entityManager.query(
                `
            SELECT *
            FROM project.vote
            WHERE identity = crypt($1, identity) AND "projectId" = $2;
        `,
                [identityString, projectId],
            )
        )[0];
    }

    async getVotesByProjectId(projectId: number): Promise<Vote[]> {
        const entityManager = getManager();

        return await entityManager.query(
            `
            SELECT * 
            FROM project.vote
            WHERE "projectId" = $1
        `,
            [projectId],
        );
    }

    async removeVotesByProjectId(projectId: number): Promise<boolean> {
        const votes = await this.getVotesByProjectId(projectId);

        return !!(await this.voteRepository.remove(votes));
    }
}
