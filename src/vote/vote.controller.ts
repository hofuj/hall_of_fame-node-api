import { Body, Controller, Param, Post } from '@nestjs/common';
import { ApiBadRequestResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { CurrentIdentity } from '../common/decorators/current-identity.decorator';
import { BooleanResponse } from '../common/DTOs/boolean-response.dto';
import { VoteDto } from './DTOs/vote.dto';
import { VoteService } from './vote.service';

@ApiTags('vote')
@Controller('vote')
export class VoteController {
    constructor(private readonly voteService: VoteService) {}

    @Post(':id')
    @ApiOkResponse({
        type: BooleanResponse,
        description: 'Vote was successful',
    })
    @ApiBadRequestResponse({
        description: 'You already voted like this on this project! || You have never voted on this project.',
    })
    async vote(@CurrentIdentity() identity, @Param('id') projectId: number, @Body() voteDto: VoteDto) {
        return {
            success: await this.voteService.vote(identity, Number(voteDto.value), projectId),
        };
    }
}
