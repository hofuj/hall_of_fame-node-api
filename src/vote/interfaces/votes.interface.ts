export interface VotesInterface {
    projectId: number;
    votes: number;
}
