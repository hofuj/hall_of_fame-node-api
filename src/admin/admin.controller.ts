import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Put,
    UploadedFiles,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import {
    ApiBadRequestResponse,
    ApiForbiddenResponse,
    ApiNotFoundResponse,
    ApiOkResponse,
    ApiTags,
    ApiUnauthorizedResponse,
} from '@nestjs/swagger';

import { LoggedUserDto } from '../auth/DTOs/logged-user-dto';
import { CurrentUser } from '../common/decorators/current-user.decorator';
import { BooleanResponse } from '../common/DTOs/boolean-response.dto';
import { BusinessErrorMessage } from '../common/enums/business-error-message.enum';
import { AcceptProjectDto } from '../project/DTOs/accept-project.dto';
import { CreateProjectDto } from '../project/DTOs/create-project.dto';
import { EditProjectDto } from '../project/DTOs/edit-project.dto';
import { ProjectDto } from '../project/DTOs/project.dto';
import { ProjectService } from '../project/project.service';
import { AdminService } from './admin.service';
import { ActivateAccountDto } from './DTOs/activate-account.dto';
import { UserActivatedGuard } from './guards/user-activated.guard';

@ApiTags('admin')
@Controller('admin')
export class AdminController {
    constructor(private readonly adminService: AdminService, private readonly projectService: ProjectService) {}

    @Post('/activate')
    @UseGuards(AuthGuard('jwt'))
    @ApiOkResponse({
        type: LoggedUserDto,
        description: 'Admin was activated successfully.',
    })
    @ApiForbiddenResponse({
        description: `${BusinessErrorMessage.PASSWORDS_DONT_MATCH} || ${BusinessErrorMessage.USER_ALREADY_ACTIVE}`,
    })
    async activateAdminAccount(
        @CurrentUser() user,
        @Body() activateAccountDto: ActivateAccountDto,
    ): Promise<LoggedUserDto> {
        return await this.adminService.activateAdminAccount(user.id, activateAccountDto);
    }

    @Post('/project')
    @UseGuards(AuthGuard('jwt'), UserActivatedGuard)
    @UseInterceptors(AnyFilesInterceptor())
    @ApiOkResponse({
        description: 'Project has been added successfully',
        type: CreateProjectDto,
    })
    @ApiUnauthorizedResponse({
        description: 'User is not logged in',
    })
    @ApiForbiddenResponse({
        description: 'User is not activated as an admin',
    })
    @ApiBadRequestResponse({
        description: 'Wrong input',
    })
    async addProject(
        @Body() createProjectDto: CreateProjectDto,
        @UploadedFiles() files?: Express.Multer.File[],
    ): Promise<ProjectDto> {
        return await this.projectService.create(createProjectDto, true, null, files);
    }

    @Post('/project/accept')
    @UseGuards(AuthGuard('jwt'), UserActivatedGuard)
    @ApiOkResponse({
        description: 'Project has been accepted successfully',
        type: BooleanResponse,
    })
    @ApiNotFoundResponse({
        description: 'Project with given id does not exist',
    })
    @ApiForbiddenResponse({
        description: 'Project is accepted already',
    })
    async acceptProject(@Body() acceptProjectDto: AcceptProjectDto): Promise<BooleanResponse> {
        return {
            success: await this.projectService.accept(acceptProjectDto.id),
        };
    }

    @Post('/project/deny')
    @UseGuards(AuthGuard('jwt'), UserActivatedGuard)
    @ApiOkResponse({
        description: 'Project has been denied successfully',
        type: BooleanResponse,
    })
    @ApiNotFoundResponse({
        description: 'Project with given id does not exist',
    })
    @ApiForbiddenResponse({
        description: 'Project is accepted already',
    })
    async denyProject(@Body() denyProjectDto: AcceptProjectDto): Promise<BooleanResponse> {
        return {
            success: await this.projectService.deny(denyProjectDto.id),
        };
    }

    @Put('/project/:id')
    @UseGuards(AuthGuard('jwt'), UserActivatedGuard)
    @UseInterceptors(AnyFilesInterceptor())
    @ApiOkResponse({
        description: 'Project was edited successfully',
        type: ProjectDto,
    })
    @ApiBadRequestResponse({
        description: 'Wrong input',
    })
    async editProject(
        @Param('id') id: number,
        @Body() editProjectDto: EditProjectDto,
        @UploadedFiles() files: Express.Multer.File[],
    ): Promise<ProjectDto> {
        return await this.projectService.edit(id, editProjectDto, files);
    }

    @Get('/project/requested')
    @UseGuards(AuthGuard('jwt'), UserActivatedGuard)
    @ApiOkResponse({
        description: 'Requested Projects were retrieved successfully',
        type: [ProjectDto],
    })
    async getRequestedProjects(): Promise<ProjectDto[]> {
        return await this.projectService.findAllRequested();
    }

    @Get('/project/requested/:id')
    @UseGuards(AuthGuard('jwt'), UserActivatedGuard)
    @ApiOkResponse({
        description: 'Requested Projects were retrieved successfully',
        type: ProjectDto,
    })
    async getRequestedProject(@Param('id') id: number): Promise<ProjectDto> {
        return await this.projectService.findRequested(id);
    }

    @Delete('/project/:id')
    @UseGuards(AuthGuard('jwt'), UserActivatedGuard)
    @ApiOkResponse({
        description: 'Requested Projects were retrieved successfully',
        type: [ProjectDto],
    })
    @ApiNotFoundResponse({
        description: 'Project with the given id does not exist',
        type: BooleanResponse,
    })
    async removeProject(@Param('id') id: number): Promise<BooleanResponse> {
        return {
            success: await this.projectService.remove(id),
        };
    }
}
