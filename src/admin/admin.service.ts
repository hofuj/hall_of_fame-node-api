import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';

import { AuthService } from '../auth/auth.service';
import { LoggedUserDto } from '../auth/DTOs/logged-user-dto';
import { BusinessErrorMessage } from '../common/enums/business-error-message.enum';
import { ActivateAccountDto } from './DTOs/activate-account.dto';
import { User } from './user.entity';

@Injectable()
export class AdminService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly authService: AuthService,
    ) {}

    async createAdmin(): Promise<boolean> {
        const adminExists = await this.userRepository.findOne({
            where: { username: process.env.ADMIN_USERNAME },
        });

        if (adminExists) {
            return true;
        }

        const password = await this.authService.hashPassword(process.env.ADMIN_PASSWORD);

        const user = new User();
        user.username = process.env.ADMIN_USERNAME;
        user.password = password;

        return !!(await this.userRepository.save(user));
    }

    async adminIsActivated(userId: number): Promise<boolean> {
        const admin = await this.userRepository.findOne({
            where: { id: userId },
        });

        if (!admin) {
            throw new HttpException(BusinessErrorMessage.USER_NOT_EXISTS, HttpStatus.NOT_FOUND);
        }

        return admin.isActive;
    }

    @Transactional()
    async activateAdminAccount(userId: number, activateAccountDto: ActivateAccountDto): Promise<LoggedUserDto> {
        if (activateAccountDto.password !== activateAccountDto.retypedPassword) {
            throw new HttpException(BusinessErrorMessage.PASSWORDS_DONT_MATCH, HttpStatus.FORBIDDEN);
        }

        if (await this.adminIsActivated(userId)) {
            throw new HttpException(BusinessErrorMessage.USER_ALREADY_ACTIVE, HttpStatus.FORBIDDEN);
        }

        const password = await this.authService.hashPassword(activateAccountDto.password);

        await this.userRepository.update({ id: userId }, { password: password, isActive: true });

        const user = await this.userRepository.findOne({ where: { id: userId } });

        return this.authService.login(user);
    }
}
