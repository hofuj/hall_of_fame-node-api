import { ApiProperty } from '@nestjs/swagger';
import { Length } from 'class-validator';

export class ActivateAccountDto {
    @ApiProperty()
    @Length(5)
    password: string;

    @ApiProperty()
    retypedPassword: string;
}
