import { ApiProperty } from '@nestjs/swagger';
import { Length } from 'class-validator';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ schema: 'usr' })
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @ApiProperty()
    @Length(5)
    username: string;

    @Column()
    @ApiProperty()
    @Length(5)
    password: string;

    @Column({ name: 'isActive', type: 'boolean', default: false })
    isActive: boolean;
}
