import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from '@nestjs/common';

import { BusinessErrorMessage } from '../../common/enums/business-error-message.enum';
import { AdminService } from '../admin.service';

@Injectable()
export class UserActivatedGuard implements CanActivate {
    constructor(private readonly adminService: AdminService) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();

        if (!request.user) {
            throw new HttpException(BusinessErrorMessage.NOT_LOGGED_IN, HttpStatus.UNAUTHORIZED);
        }

        const { id } = request.user;

        const isActivated = await this.adminService.adminIsActivated(id);

        if (!isActivated) {
            throw new HttpException(BusinessErrorMessage.USER_NOT_ACTIVE, HttpStatus.FORBIDDEN);
        }

        return true;
    }
}
