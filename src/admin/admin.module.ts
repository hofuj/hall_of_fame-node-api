import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthModule } from '../auth/auth.module';
import { CategoryModule } from '../category/category.module';
import { ProjectModule } from '../project/project.module';
import { AdminController } from './admin.controller';
import { AdminService } from './admin.service';
import { User } from './user.entity';

@Module({
    imports: [TypeOrmModule.forFeature([User]), AuthModule, ProjectModule, CategoryModule],
    providers: [AdminService],
    exports: [AdminService],
    controllers: [AdminController],
})
export class AdminModule {}
