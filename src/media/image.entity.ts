import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Project } from '../project/project.entity';

@Entity({ schema: 'media' })
export class Image {
    @PrimaryGeneratedColumn()
    @ApiProperty()
    id: number;

    @Column()
    @ApiProperty()
    url: string;

    @ManyToOne(() => Project, (project) => project.images)
    project: Project;
}
