import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CloudinaryModule } from '../cloudinary/cloudinary.module';
import { Image } from './image.entity';
import { MediaService } from './media.service';

@Module({
    imports: [TypeOrmModule.forFeature([Image]), CloudinaryModule],
    providers: [MediaService],
    exports: [MediaService],
})
export class MediaModule {}
