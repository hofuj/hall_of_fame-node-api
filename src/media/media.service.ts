import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';

import { CloudinaryService } from '../cloudinary/cloudinary.service';
import { Project } from '../project/project.entity';
import { Image } from './image.entity';

@Injectable()
export class MediaService {
    constructor(
        private readonly cloudinaryService: CloudinaryService,
        @InjectRepository(Image) readonly imageRepository: Repository<Image>,
    ) {}

    async createImages(files: Express.Multer.File[], project: Project): Promise<Image[]> {
        const images = [];
        const promises = files.map(async (file) => {
            const image = new Image();
            image.url = await this.uploadImage(file);
            image.project = project;
            images.push(await this.imageRepository.save(image));
        });
        await Promise.all(promises);
        return images;
    }

    private async uploadImage(file: Express.Multer.File): Promise<string> {
        try {
            const uploadedFileData = await this.cloudinaryService.uploadImage(file);
            return uploadedFileData.url;
        } catch (e) {
            throw new HttpException(
                'Something went wrong with the file upload. Check your data type and try again.',
                HttpStatus.BAD_REQUEST,
            );
        }
    }

    async getImagesByProjectId(projectId: number): Promise<Image[]> {
        const entityManager = getManager();

        return await entityManager.query(
            `
            SELECT * 
            FROM media.image
            WHERE "projectId" = $1
        `,
            [projectId],
        );
    }

    async updateImages(imageUrls: string[], project: Project, files: Express.Multer.File[]): Promise<boolean> {
        const currentImages = await this.getImagesByProjectId(project.id);
        const imagesToDelete = await currentImages.filter((currImage) => !imageUrls.includes(currImage.url));

        await Promise.all(imagesToDelete.map((image) => this.imageRepository.remove(image)));

        return !!(await this.createImages(files, project));
    }

    async removeImagesByProjectId(projectId: number): Promise<boolean> {
        const images = await this.getImagesByProjectId(projectId);

        return !!(await this.imageRepository.remove(images));
    }
}
