import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { Category } from './category.entity';
import { CategoryService } from './category.service';

@ApiTags('Category')
@Controller('category')
export class CategoryController {
    constructor(private readonly categoryService: CategoryService) {}

    @Get()
    @ApiOkResponse({
        description: 'All categories were retrieved successfully',
        type: [Category],
    })
    async getAllCategories(): Promise<Category[]> {
        return await this.categoryService.getAllCategories();
    }
}
