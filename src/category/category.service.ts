import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { BusinessErrorMessage } from '../common/enums/business-error-message.enum';
import { Category } from './category.entity';

@Injectable()
export class CategoryService {
    constructor(@InjectRepository(Category) private readonly categoryRepository: Repository<Category>) {}

    async getCategoryByName(name: string): Promise<Category> {
        const category = await this.categoryRepository.findOne({ name });

        if (!category) {
            throw new HttpException(BusinessErrorMessage.CATEGORY_DOES_NOT_EXIST, HttpStatus.NOT_FOUND);
        }

        return category;
    }

    async getAllCategories(): Promise<Category[]> {
        return await this.categoryRepository.find();
    }
}
