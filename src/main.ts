import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { initializeTransactionalContext } from 'typeorm-transactional-cls-hooked';

import { AdminService } from './admin/admin.service';
import { AppModule } from './app.module';

async function bootstrap() {
    initializeTransactionalContext();
    const app = await NestFactory.create(AppModule);
    app.useGlobalPipes(new ValidationPipe());

    app.enableCors();

    const adminService: AdminService = app.get<AdminService>(AdminService);
    await adminService.createAdmin();

    const config = new DocumentBuilder()
        .setTitle('Hall of Fame')
        .setDescription('Hall of Fame API Description')
        .setVersion('1.0')
        .addTag('admin')
        .addTag('project')
        .build();
    const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('api', app, document);

    await app.listen(process.env.PORT || 3000);
}

bootstrap();
