import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AuthModule } from '../auth/auth.module';
import { CategoryModule } from '../category/category.module';
import { MediaModule } from '../media/media.module';
import { TeamModule } from '../team/team.module';
import { VoteModule } from '../vote/vote.module';
import { ProjectRequest } from './project-request.entity';
import { ProjectController } from './project.controller';
import { Project } from './project.entity';
import { ProjectService } from './project.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([Project, ProjectRequest]),
        TeamModule,
        MediaModule,
        VoteModule,
        CategoryModule,
        AuthModule,
    ],
    providers: [ProjectService],
    exports: [ProjectService],
    controllers: [ProjectController],
})
export class ProjectModule {}
