import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Category } from '../category/category.entity';
import { Image } from '../media/image.entity';
import { Team } from '../team/team.entity';
import { Vote } from '../vote/vote.entity';

@Entity({ schema: 'project' })
export class Project {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @Column({ name: 'updatedAt', type: 'timestamp', default: () => 'NOW()' })
    updatedAt: Date;

    @Column({ name: 'isAccepted', type: 'boolean', default: false })
    isAccepted: boolean;

    @Column({ name: 'codeUrl', type: 'varchar', nullable: true })
    codeUrl?: string;

    @Column({ name: 'websiteUrl', type: 'varchar', nullable: true })
    websiteUrl?: string;

    currentUserVote: number;

    @OneToOne(() => Team, (team) => team.project, { eager: true, cascade: true })
    @JoinColumn()
    team: Team;

    @OneToMany(() => Image, (image) => image.project, { eager: true })
    images: Image[];

    @OneToMany(() => Vote, (vote) => vote.project)
    votes?: Vote[];

    voteCount: number;

    @ManyToOne(() => Category, (category) => category.projects, { eager: true })
    category: Category;
}
