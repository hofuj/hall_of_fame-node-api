import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';
import { Transactional } from 'typeorm-transactional-cls-hooked';

import { AuthService } from '../auth/auth.service';
import { CategoryService } from '../category/category.service';
import { BusinessErrorMessage } from '../common/enums/business-error-message.enum';
import { Identity } from '../common/interfaces/identity.interface';
import { MediaService } from '../media/media.service';
import { CreateTeamDto } from '../team/DTOs/create-team-dto';
import { TeamService } from '../team/team.service';
import { VoteService } from '../vote/vote.service';
import { CreateProjectDto } from './DTOs/create-project.dto';
import { EditProjectDto } from './DTOs/edit-project.dto';
import { ProjectDto } from './DTOs/project.dto';
import { ProjectRequest } from './project-request.entity';
import { Project } from './project.entity';

@Injectable()
export class ProjectService {
    // private readonly PROJECT_REQUEST_DAY_LIMIT = 7;
    private readonly PROJECT_REQUEST_PROJECTS_LIMIT = 3;
    private readonly MAX_IMAGES_PER_PROJECT = 5;

    constructor(
        @InjectRepository(Project) private readonly projectRepository: Repository<Project>,
        @InjectRepository(ProjectRequest) private readonly projectRequestRepository: Repository<ProjectRequest>,
        private readonly teamService: TeamService,
        private readonly mediaService: MediaService,
        private readonly voteService: VoteService,
        private readonly categoryService: CategoryService,
        private readonly authService: AuthService,
    ) {}

    @Transactional()
    async create(
        createProjectDto: CreateProjectDto,
        admin: boolean,
        identity?: Identity,
        files?: Express.Multer.File[],
    ): Promise<ProjectDto> {
        if (files?.length >= this.MAX_IMAGES_PER_PROJECT) {
            throw new HttpException(BusinessErrorMessage.TOO_MANY_IMAGES, HttpStatus.BAD_REQUEST);
        }

        const project = this.projectRepository.create({
            name: createProjectDto.name,
            description: createProjectDto.description,
            isAccepted: admin,
            codeUrl: createProjectDto.codeUrl,
            websiteUrl: createProjectDto.websiteUrl,
        });

        project.category = await this.categoryService.getCategoryByName(createProjectDto.category);

        const createTeamDto = new CreateTeamDto(createProjectDto.teamName, createProjectDto.teamMembers);

        project.team = await this.teamService.create(createTeamDto);

        const savedProject = await this.projectRepository.save(project);

        if (files) {
            await this.mediaService.createImages(files, savedProject);
        }

        if (!admin) {
            await this.createIdentityRequest(identity);
        }

        return new ProjectDto(savedProject);
    }

    @Transactional()
    async edit(id: number, editProjectDto: EditProjectDto, files: Express.Multer.File[]): Promise<ProjectDto> {
        let project = await this.projectRepository.findOne(id);

        if (!project) {
            throw new HttpException(BusinessErrorMessage.PROJECT_DOES_NOT_EXIST, HttpStatus.NOT_FOUND);
        }

        await this.projectRepository.update(
            { id },
            {
                name: editProjectDto.name,
                description: editProjectDto.description,
                category: await this.categoryService.getCategoryByName(editProjectDto.category),
                codeUrl: editProjectDto.codeUrl,
                websiteUrl: editProjectDto.websiteUrl,
            },
        );

        await this.teamService.updateTeam(id, editProjectDto.teamName, editProjectDto.teamMembers);
        await this.mediaService.updateImages(editProjectDto.imageUrls, project, files);

        project = await this.projectRepository.findOne(id);

        return new ProjectDto(project);
    }

    @Transactional()
    async findAll(identity?: Identity): Promise<ProjectDto[]> {
        const voteCounts = await this.voteService.getVoteCounts();

        const votedValues = await this.voteService.getUserVotes(this.authService.getIdentityString(identity));

        const projects = await this.projectRepository.find({
            where: { isAccepted: true },
            order: { updatedAt: 'DESC' },
        });

        const projectsDto = projects.map((p) => {
            const votes = voteCounts.find((v) => v.projectId === p.id)?.votes;

            p.voteCount = votes ?? 0;
            const vote = votedValues.find((v) => v.projectId === p.id);
            p.currentUserVote = vote && identity ? vote.value : 0;
            return new ProjectDto(p);
        });

        return projectsDto.sort((a, b) => b.voteCount - a.voteCount);
    }

    async findAllRequested(): Promise<ProjectDto[]> {
        const projects = await this.projectRepository.find({
            where: { isAccepted: false },
            order: { updatedAt: 'DESC' },
        });

        return projects.map((p) => {
            p.voteCount = 0;
            return new ProjectDto(p);
        });
    }

    async findRequested(projectId: number): Promise<ProjectDto> {
        const project = (
            await this.projectRepository.find({
                where: { isAccepted: false, id: projectId },
            })
        )[0];

        if (!project) {
            throw new HttpException(BusinessErrorMessage.PROJECT_DOES_NOT_EXIST, HttpStatus.NOT_FOUND);
        }

        project.voteCount = 0;
        return new ProjectDto(project);
    }

    @Transactional()
    async findOne(id: number, identity?: Identity): Promise<ProjectDto> {
        const project = (
            await this.projectRepository.find({
                where: { id, isAccepted: true },
            })
        )[0];

        if (!project) {
            throw new HttpException(BusinessErrorMessage.PROJECT_DOES_NOT_EXIST, HttpStatus.NOT_FOUND);
        }

        project.voteCount = await this.voteService.getVoteCountByProjectId(id);
        project.currentUserVote = identity
            ? await this.voteService.getUserVoteValue(this.authService.getIdentityString(identity), id)
            : 0;
        return new ProjectDto(project);
    }

    async accept(id: number): Promise<boolean> {
        const project = await this.projectRepository.findOne(id);

        if (!project) {
            throw new HttpException(BusinessErrorMessage.PROJECT_DOES_NOT_EXIST, HttpStatus.NOT_FOUND);
        }

        if (project.isAccepted) {
            throw new HttpException(BusinessErrorMessage.PROJECT_ALREADY_ACCEPTED, HttpStatus.FORBIDDEN);
        }

        const update = await this.projectRepository.update({ id }, { isAccepted: true });

        return update.affected > 0;
    }

    async deny(id: number): Promise<boolean> {
        const project = await this.projectRepository.findOne(id);

        if (!project) {
            throw new HttpException(BusinessErrorMessage.PROJECT_DOES_NOT_EXIST, HttpStatus.NOT_FOUND);
        }

        if (project.isAccepted) {
            throw new HttpException(BusinessErrorMessage.PROJECT_ALREADY_ACCEPTED, HttpStatus.FORBIDDEN);
        }

        await this.mediaService.removeImagesByProjectId(project.id);

        await this.voteService.removeVotesByProjectId(project.id);

        return !!(await this.projectRepository.remove(project));
    }

    async createIdentityRequest(identity: Identity): Promise<boolean> {
        const entityManager = getManager();

        const identityString = this.authService.getIdentityString(identity);

        const requests = await entityManager.query(
            `
            SELECT *
            FROM project.project_request
            WHERE identity = crypt($1, identity) AND "updatedAt"::date >= (CURRENT_DATE - 7)
            ORDER BY "updatedAt" DESC
            LIMIT $2;
        `,
            [identityString, this.PROJECT_REQUEST_PROJECTS_LIMIT],
        );

        if (requests.length >= this.PROJECT_REQUEST_PROJECTS_LIMIT) {
            throw new HttpException(BusinessErrorMessage.PROJECT_RATE_LIMIT, HttpStatus.FORBIDDEN);
        }

        const identityHash = await this.authService.hashIdentityString(identityString);

        const projectRequest = this.projectRequestRepository.create({
            identity: identityHash,
        });

        return !!(await this.projectRequestRepository.save(projectRequest));
    }

    async remove(projectId: number): Promise<boolean> {
        const project = await this.projectRepository.findOne(projectId);

        if (!project) {
            throw new HttpException(BusinessErrorMessage.PROJECT_DOES_NOT_EXIST, HttpStatus.NOT_FOUND);
        }

        await this.mediaService.removeImagesByProjectId(project.id);

        await this.voteService.removeVotesByProjectId(project.id);

        return !!(await this.projectRepository.remove(project));
    }
}
