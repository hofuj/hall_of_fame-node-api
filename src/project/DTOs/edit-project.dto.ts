import { ApiProperty } from '@nestjs/swagger';

import { CreateProjectDto } from './create-project.dto';

export class EditProjectDto extends CreateProjectDto {
    @ApiProperty()
    imageUrls: string[];
}
