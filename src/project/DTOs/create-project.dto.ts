import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Length } from 'class-validator';

import { CreateTeamMemberDto } from '../../team/DTOs/create-team-member';

export class CreateProjectDto {
    @ApiProperty()
    @Length(5, 100)
    name: string;

    @ApiPropertyOptional()
    teamName?: string;

    @ApiPropertyOptional({ type: [CreateTeamMemberDto] })
    teamMembers?: CreateTeamMemberDto[];

    @ApiPropertyOptional()
    description?: string;

    @ApiProperty()
    category: string;

    @ApiPropertyOptional()
    codeUrl?: string;

    @ApiPropertyOptional()
    websiteUrl?: string;
}
