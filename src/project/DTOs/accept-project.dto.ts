import { ApiProperty } from '@nestjs/swagger';

export class AcceptProjectDto {
    @ApiProperty()
    id: number;
}
