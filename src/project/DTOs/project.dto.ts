import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';

import { Category } from '../../category/category.entity';
import { Image } from '../../media/image.entity';
import { Team } from '../../team/team.entity';
import { Project } from '../project.entity';

export class ProjectDto {
    @ApiProperty()
    id: number;

    @ApiProperty()
    name: string;

    @ApiPropertyOptional()
    description: string;

    @ApiProperty()
    updatedAt: Date;

    @ApiProperty()
    isAccepted: boolean;

    @ApiPropertyOptional()
    codeUrl?: string;

    @ApiPropertyOptional()
    websiteUrl?: string;

    @ApiProperty({ type: Team })
    team: Team;

    @ApiProperty({ type: [Image] })
    images: Image[];

    @ApiProperty()
    voteCount: number;

    @ApiProperty()
    currentUserVote: number;

    @ApiProperty()
    category: Category;

    constructor(project: Project) {
        this.id = project.id;
        this.name = project.name;
        this.description = project.description;
        this.updatedAt = project.updatedAt;
        this.isAccepted = project.isAccepted;
        this.codeUrl = project.codeUrl;
        this.websiteUrl = project.websiteUrl;
        this.team = project.team;
        this.images = project.images;
        this.voteCount = project.voteCount;
        this.category = project.category;
        this.currentUserVote = project.currentUserVote;
    }
}
