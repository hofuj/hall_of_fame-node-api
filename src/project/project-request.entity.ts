import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ schema: 'project' })
export class ProjectRequest {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    identity: string;

    @Column({ name: 'updatedAt', type: 'timestamp', default: () => 'NOW()' })
    updatedAt: Date;
}
