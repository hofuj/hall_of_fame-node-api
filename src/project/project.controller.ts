import { Body, Controller, Get, Param, Post, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { ApiBadRequestResponse, ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { CurrentIdentity } from '../common/decorators/current-identity.decorator';
import { BusinessErrorMessage } from '../common/enums/business-error-message.enum';
import { CreateProjectDto } from './DTOs/create-project.dto';
import { ProjectDto } from './DTOs/project.dto';
import { ProjectService } from './project.service';

@ApiTags('project')
@Controller('project')
export class ProjectController {
    constructor(private readonly projectService: ProjectService) {}

    @Get()
    @ApiOkResponse({
        type: [ProjectDto],
        description: 'Projects were retrieved successfully',
    })
    async getAll(@CurrentIdentity() identity): Promise<ProjectDto[]> {
        return await this.projectService.findAll(identity);
    }

    @Get(':id')
    @ApiOkResponse({
        type: ProjectDto,
        description: 'Project was retrieved successfully',
    })
    @ApiNotFoundResponse({
        description: BusinessErrorMessage.PROJECT_DOES_NOT_EXIST,
    })
    async getOne(@Param('id') id: number, @CurrentIdentity() identity): Promise<ProjectDto> {
        return await this.projectService.findOne(id, identity);
    }

    @Post()
    @UseInterceptors(AnyFilesInterceptor())
    @ApiOkResponse({
        description: 'Project has been added successfully',
        type: CreateProjectDto,
    })
    @ApiOkResponse({
        description: 'Project has been added successfully',
        type: CreateProjectDto,
    })
    @ApiBadRequestResponse({
        description: 'Wrong input',
    })
    async addProject(
        @CurrentIdentity() identity,
        @Body() createProjectDto: CreateProjectDto,
        @UploadedFiles() files?: Array<Express.Multer.File>,
    ): Promise<ProjectDto> {
        return await this.projectService.create(createProjectDto, false, identity, files);
    }
}
