import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getManager, Repository } from 'typeorm';

import { CreateTeamDto } from './DTOs/create-team-dto';
import { CreateTeamMemberDto } from './DTOs/create-team-member';
import { Member } from './member.entity';
import { Team } from './team.entity';

@Injectable()
export class TeamService {
    constructor(
        @InjectRepository(Team)
        private readonly teamRepository: Repository<Team>,
        @InjectRepository(Member)
        private readonly memberRepository: Repository<Member>,
    ) {}

    async create(createTeamDto: CreateTeamDto): Promise<Team> {
        const team = this.teamRepository.create({
            name: createTeamDto.name,
            members: await this.createTeamMembers(createTeamDto.teamMembers),
        });

        return await this.teamRepository.save(team);
    }

    async getTeamMembersByProjectId(projectId: number): Promise<Member[]> {
        const entityManager = getManager();
        return await entityManager.query(
            `
            SELECT m.*
            FROM project.project p
            INNER JOIN team.team t ON t.id = p."teamId"
            INNER JOIN team.team_members_member tm ON tm."teamId" = t.id
            INNER JOIN team.member m ON tm."memberId" = m.id
            WHERE p.id = $1
        `,
            [projectId],
        );
    }

    async removeTeamMembersByProjectId(projectId: number): Promise<boolean> {
        const team = await this.getTeamByProjectId(projectId);

        return !!(await this.teamRepository.remove(team));
    }

    async getTeamByProjectId(projectId: number): Promise<Team> {
        const entityManager = getManager();
        return (
            await entityManager.query(
                `
            SELECT t.*
            FROM project.project p
            INNER JOIN team.team t ON t.id = p."teamId"
            WHERE p.id = $1
        `,
                [projectId],
            )
        )[0];
    }

    async updateTeam(projectId: number, teamName?: string, teamMembers?: CreateTeamMemberDto[]) {
        const team = await this.getTeamByProjectId(projectId);
        team.name = teamName;

        const currentTeamMembers = await this.getTeamMembersByProjectId(projectId);

        team.members = currentTeamMembers.filter((c) => {
            const exists = teamMembers.filter((t) => t.name == c.name && t.surname == c.surname);
            return exists.length > 0;
        });

        const membersToAdd = teamMembers.filter((c) => {
            const exists = currentTeamMembers.filter((t) => t.name == c.name && t.surname == c.surname);
            return exists.length === 0;
        });

        team.members = team.members.concat(
            membersToAdd.map((member) => {
                return this.memberRepository.create({
                    name: member.name,
                    surname: member.surname,
                    personalWebsiteUrl: member.personalWebsiteUrl,
                });
            }),
        );

        return await this.teamRepository.save(team);
    }

    private async createTeamMembers(teamMembers?: CreateTeamMemberDto[]): Promise<Member[]> {
        const membersToAdd = teamMembers.map((member) => {
            return this.memberRepository.create({
                name: member.name,
                surname: member.surname,
                personalWebsiteUrl: member.personalWebsiteUrl,
            });
        });

        return await this.memberRepository.save(membersToAdd);
    }
}
