import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, JoinTable, ManyToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { Project } from '../project/project.entity';
import { Member } from './member.entity';

@Entity({ schema: 'team' })
export class Team {
    @PrimaryGeneratedColumn()
    @ApiProperty()
    id: number;

    @Column({ nullable: true })
    @ApiProperty()
    name: string;

    @ManyToMany(() => Member, { eager: true, cascade: true })
    @JoinTable()
    @ApiProperty({ type: [Member] })
    members: Member[];

    @OneToOne(() => Project, (project) => project.team)
    project: Project;
}
