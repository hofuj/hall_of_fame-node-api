import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Member } from './member.entity';
import { Team } from './team.entity';
import { TeamService } from './team.service';

@Module({
    imports: [TypeOrmModule.forFeature([Team, Member])],
    providers: [TeamService],
    exports: [TeamService],
})
export class TeamModule {}
