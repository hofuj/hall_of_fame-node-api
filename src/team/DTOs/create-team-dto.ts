import { CreateTeamMemberDto } from './create-team-member';

export class CreateTeamDto {
    name?: string;
    teamMembers: CreateTeamMemberDto[];

    constructor(name?: string, teamMembers?: CreateTeamMemberDto[]) {
        this.name = name;
        this.teamMembers = teamMembers ?? [];
    }
}
