import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { Length } from 'class-validator';

export class CreateTeamMemberDto {
    @ApiProperty()
    @Length(3, 30)
    name: string;

    @ApiProperty()
    @Length(3, 100)
    surname: string;

    @ApiPropertyOptional()
    personalWebsiteUrl?: string;
}
