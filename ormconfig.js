module.exports = {
    type: 'postgres',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USER,
    password: process.env.DB_PW,
    database: process.env.DB_NAME,
    synchronize: false,
    entities: ['dist/**/*.entity{ .ts,.js}'],
    keepConnectionAlive: true,
    autoLoadEntities: true,
    migrations: ['dist/migration/*{.ts,.js}'],
    migrationsTableName: 'migrations_typeorm',
    migrationsRun: true,
    cli: {
        migrationsDir: 'src/migration',
    },
};
